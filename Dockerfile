# 基础镜像使用java
FROM openjdk:8
# 作者
MAINTAINER galen
# VOLUME 指定临时文件目录为/tmp，在主机/var/lib/docker目录下创建了一个临时文件并链接到容器的/tmp
VOLUME /tmp
# 定义变量接收参数
ENV PARAMS=""
# 将jar包添加到容器中
ADD /target/barrage.jar barrage.jar
# 运行jar包
RUN bash -c 'touch /barrage.jar'

ENTRYPOINT ["sh","-c","java -jar $JAVA_OPTS /barrage.jar $PARAMS"]
#暴露端口
EXPOSE 9999
