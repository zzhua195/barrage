package top.houry.netty.barrage.bo;

import lombok.Data;

@Data
public class BarrageChannelAttrInfo {
    private String videoId;
}
