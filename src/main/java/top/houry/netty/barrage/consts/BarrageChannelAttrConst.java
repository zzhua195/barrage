package top.houry.netty.barrage.consts;

import io.netty.util.AttributeKey;
import top.houry.netty.barrage.bo.BarrageChannelAttrInfo;

public interface BarrageChannelAttrConst {

    AttributeKey<BarrageChannelAttrInfo> NETTY_CHANNEL_VIDEO_ID_ATTR = AttributeKey.valueOf("channelAttrInfo");
}
